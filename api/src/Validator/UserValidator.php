<?php

namespace App\Validator;

class UserValidator
{

	/**
	* Simple validation if all params are set. Could be done in much robust way. Just use reflection to check if all properties from Entity and decoded json are set.
	* @param \stdClass $sourceObj - decoded json response
	*/
	public function isValid(\stdClass $sourceObj)
	{
		$isValid = true;
		$originReflectionObj = new \ReflectionClass('App\Entity\User');
		$properties = $originReflectionObj->getProperties();
		
		foreach($properties as $property)
		{
			//ID is not need, but as we are using Entity we need to filter it out (I was not successful to find the way how to not display it here. Solution should be to have separate entity for response validation and Entity mapping. But I did not implement it here)
			//ROLE param is skipped too. Can be validated or preset from default. but I just want to make it simple here.
			if($property->name != 'id' && $property->name != 'role') {
				if(!property_exists($sourceObj, $property->name) || empty($sourceObj->{$property->name})) {
					$isValid = false;
				}
			}
		}

		return $isValid;
	}
}