<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\User;
use App\Validator\UserValidator;



class DefaultController extends Controller
{
    /** 
     * @Route("/api/users", name="user", methods={"GET"})
     * @SWG\Get(
     *     path="/api/users",
     *     summary="List users",
     *     tags={"get_users"},
     *     description="Get list of all registered users",
     *     operationId="getAllUsers",
     *     produces={"application/xml", "application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(@Model(type=User::class))
     *         ),
     *     )
     * )
     */
	public function getUsers(EntityManagerInterface $em, SerializerInterface $serializer)
	{
        $res = $em->getRepository(User::class)->findAll();
        
        $responseArray = ["status" => 'ok', "message" => $res];

        return new Response($serializer->serialize($responseArray,'json'));
	}

    /**
     *
     * @Route("/api/users", name="register_user", methods={"POST"})
     * @SWG\Post(
     *     path="/api/users",
     *     summary="List users",
     *     tags={"register_user"},
     *     description="Register user",
     *     operationId="registerUser",
     *     produces={"application/xml", "application/json"},
     *     @SWG\Parameter(
     *         name="user",
     *         in="body",
     *         description="user entity",
     *         required=true,
     *         type="string",
     *         @SWG\Schema(ref="#/definitions/User")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation"
     *     )
     * )
     *
     */
    public function registerUser(Request $request, EntityManagerInterface $em, SerializerInterface $serializer, UserValidator $validator)
    {

        $userObj = json_decode($request->getContent());

        //if response is valid json
        if($userObj) {
            if($validator->isValid($userObj)) {

                $user = new User();
                $user->setUserName($userObj->userName);
                $user->setEmail($userObj->email);
                $user->setPassword($userObj->password);
                $user->setRole(User::ROLE_USER);

                //try to save to DB
                try {
                    $em->persist($user);
                    $result = $em->flush();
                    $responseArray = ["status" => 'ok', "message" => $user];
                    
                } catch (\Doctrine\ORM\ORMException $e) {
                    $responseArray = ["status" => 'error', "message" => 'DB Error'];
                }
            }
            else {
                $responseArray = ["status" => 'error', "message" => 'missing required params. check the API doc.'];
            }
        } else {
            $responseArray = ["status" => 'error', "message" => 'invalid json'];
        }

        return new Response($serializer->serialize($responseArray,'json'));

       
    }
}