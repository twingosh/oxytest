<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @SWG\Definition(definition="User", required={"userName", "password","email"}, type="object", @SWG\Xml(name="User"))
 */


class User
{
	// REVIEW: roles can be set, managed and valided in various ways (e.g. via bit operations if we have more roles)
	// here I used only simple management
	const ROLE_USER = 1;
	const ROLE_ADMIN = 2;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
     * @ORM\Column(type="string", length=255)
     * @SWG\Property()
     */
    private $userName;

    /**
     * @ORM\Column(type="string", length=255)
     * @SWG\Property()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @SWG\Property()
     */
    private $password;

    /**
     * @ORM\Column(type="smallint")
     * @SWG\Property()
     */
    private $role = self::ROLE_USER;

    
    public function getId()
    {
    	return $this->id;
    }

    public function getUserName(): string
    {
    	return $this->userName;
    }

    public function setUserName(string $userName)
    {
    	$this->userName = $userName;
    }

    public function getEmail(): string
    {
    	return $this->email;
    }

    public function setEmail(string $email)
    {
    	$this->email = $email;
    }

    public function getPassword(): string
    {
    	return $this->password;
    }

    public function setPassword(string $password)
    {
    	$this->password = $password;
    }

    public function getRole(): int
    {
    	return $this->role;
    }

    public function setRole(int $role)
    {
    	$roles = [self::ROLE_USER, self::ROLE_ADMIN];
    	$this->role = in_array($role, $roles) ? $role : self::ROLE_USER;
    }




}
