# OXY TEST README #

## HOW IT IS DONE ##

### Client ###
- project created via `composer create-project symfony/website-skeleton my-project`
- symfony `form` and `validator` used for validating data (CSRF protection included)
- `guzzle` used for sending requests to endpoint
- twitter boostrap and twig used for rendering form (bootstrap is loaded from CDN so internet connection needed to see cool form)
- just one simple unit test created.

### API ###
- used base auth for requests
- swagger is used for API doc (see your_api_endpoint_url.cz/api/doc)


## HOW TO RUN IT ##

- run `composer install` in both client/ and api/ dirs
- configure .htaccess basePath according to your URL on localhost
- config/services.yaml configure API paths based on your settings
database
- db connection must be set in .env file in your api/ dir (search DATABASE_URL)
- run `php bin/console doctrine:database:create`
- run `php bin/console doctrine:migration:migrate`

urls

localhost/oxytest/client/

localhost/oxytest/api/api/doc