# OXY TEST README #

HOW IT IS DONE

Client
- project created via `composer create-project symfony/website-skeleton my-project`
- symfony `form` and `validator` used for validating data (CSRF protection included)
- `guzzle` used for sending requests to endpoint
- twitter boostrap and twig used for rendering form (bootstrap is loaded from CDN so internet connection needed to see cool form)
- just one simple unit test created.

API
- used base auth for requests
- swagger is used for API doc (see your_api_endpoint_url.cz/api/doc)


HOW TO RUN IT

- run `composer install`
- configure .htaccess basePath if needed
- config/services.yaml configure API paths if they are different
database
- db connection must be set in .env (use .env.dist file for creation)
- run `php bin/console doctrine:database:create`
- run `php bin/console doctrine:migration:migrate`