<?php

namespace App\Tests\Client;

use PHPUnit\Framework\TestCase;

use App\Client\GuzzleClient;
use App\Client\RequestManager;
use App\Entity\User;

use GuzzleHttp\Psr7\Response;


class RequestManagerTest extends TestCase
{

	/**
	* @dataProvider getUsers
	*/
	public function testPostRequest($expectedStatus, $user, $response)
	{
		$this->clientMock = $this->createMock(GuzzleClient::class);
		$this->clientMock->expects($this->once())->method('post')
             ->willReturn($response);

		$requestManager = new RequestManager($this->clientMock);
		$status = $requestManager->sendRegistrationRequest($user);

		$this->assertEquals($expectedStatus,$status);
	}
	

	public function getUsers()
	{
		$user1 = new User();
		$user1->setUserName('Jan z Rokycan');
		$user1->setEmail('jan@zrokycan.cz');
		$user1->setPassword('asdfasdfadf');

		$response1 = new Response(200,['Content-Length' => 1],json_encode(['status'=>'ok','message'=>'xxx']));
		
		$user2 = new User();
		$user2->setUserName('Jan z Rokycan');
		$user2->setEmail('jan@zrokycan.cz');

		$response2 = new Response(400,['Content-Length' => 1],json_encode(['status'=>'error','message'=>'xxx']));
		
		return [
			[true, $user1,$response1], 
			[false, $user2, $response2]
		];
	}

	
}