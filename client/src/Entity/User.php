<?php

namespace App\Entity;


class User
{

    const USER = 1;
    const ADMIN = 2;

    private $id;

    private $userName;

    private $email;

    private $password;

    private $role;

    public function getUserName(): string
    {
    	return (string) $this->userName;
    }

    public function setUserName(string $userName)
    {
    	$this->userName = $userName;
    }

    public function getEmail(): string
    {
    	return (string) $this->email;
    }

    public function setEmail(string $email)
    {
    	$this->email = $email;
    }


    public function getPassword(): string
    {
    	return (string) $this->password;
    }

    public function setPassword(string $password)
    {
    	$this->password = password_hash($password,PASSWORD_BCRYPT);
    }

}
