<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class Registration extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       
        $builder
            ->add('userName', TextType::class, array('required' => true))
            ->add('email', EmailType::class,  array('required' => true))
            ->add('password', PasswordType::class,  array('required' => true))
            ->add('save', SubmitType::class, array('label' => 'Register user'));
    }
}