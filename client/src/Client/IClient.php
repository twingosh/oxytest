<?php

namespace App\Client;

use App\Entity\User;

interface IClient
{
	public function get();

	public function post(User $user);
}