<?php

namespace App\Client;

use App\Entity\User;

class RequestManager
{
	/**
	* @var GuzzleClient $client
	*/
	private $client;

	/**
	* @param GuzzleClient $client
	*/
	public function __construct(GuzzleClient $client)
	{
		$this->client = $client;
	}

	/**
	* Send the request using guzzleclient and parse the body
	*/
	public function sendRegistrationRequest(User $user): bool
	{
		$response = $this->client->post($user);
		$content = $this->transform($response);
		
		if($content && isset($content->status) && $content->status == 'ok'){
			return true;
		} 
		
		return false;
	}

	protected function transform(\GuzzleHttp\Psr7\Response $response)
	{
		$content = $response->getBody()->getContents();
		if($content) {
			$decodedContent = json_decode($content);
			return $decodedContent ?? null;
			
		}
		return null;
	}

	/**
	* get all users. As we have only one GET request I just simplified it to ->get()
	* in case we need to extend it. the client should be called with e.g. params or by using different methods.
	*/
	public function getAllUsers()
	{
		$response = $this->client->get();
		$content = $this->transform($response);

		return $content ?? null;
	}
	
}