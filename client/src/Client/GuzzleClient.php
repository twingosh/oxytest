<?php

namespace App\Client;

use GuzzleHttp\Client as Guzzle;

use App\Entity\User;


use Symfony\Component\Serializer\SerializerInterface;

class GuzzleClient implements IClient
{
	private $client;

	private $urlGet;

	private $urlPost;

	private $login;

	private $password;

	private $serializer;

	/**
	* urls and login credentials are stored in configuration files and are injected
	* @param Client $client
	* @param string $urlGet 
	* @param string $urlPost
	* @param string $login
	* @param string $password
	*/
	public function __construct(SerializerInterface $serializer, $urlGet, $urlPost, $login, $password)
	{
		$this->client = new Guzzle();

		$this->serializer = $serializer;
		$this->urlGet = $urlGet;
		$this->urlPost = $urlPost;
		$this->login = $login;
		$this->password = $password;
	}

	/**
	* call the only one GET request we want.
	* base auth is used to give at least some level of security.
	* @return GuzzleHttp\Psr7\Response
	*/
	public function get()
	{
		$response = $this->client->request('GET', $this->urlGet, [
    		'auth' => [$this->login, $this->password]
		]);
		return $response;
	}


	/**
	* call the only POST request we want
	* @return GuzzleHttp\Psr7\Response
	*/
	public function post(User $user)
	{
		$response = null;

		try {
		$response = $this->client->request('POST', $this->urlPost, [
    		'auth' => [$this->login, $this->password],
    		'body' => $this->serializer->serialize($user, 'json')
		]);	
		} catch(\GuzzleHttp\Exception\ServerException $e)
		{
			//TODO can trow own exception
		}
		return $response;
	}
}