<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use App\Form\Registration;
use App\Entity\User;
use App\Client\RequestManager;


class DefaultController extends Controller
{

	/**
	* @param Request $request - Symfony Request object to handle form submit
	* @param RequestManager $requestManager - manager for sending request to API
	*/
	public function index(Request $request, RequestManager $requestManager)
	{

		$userEntity = new User();
		$form = $this->createForm(Registration::class, $userEntity);

      	$form->handleRequest($request);

      	//handling form submit
		if ($form->isSubmitted() && $form->isValid()) {

	        $userFormData = $form->getData();

	        //let the form data validate and send by someone else than this controller
	        $isRegistered = $requestManager->sendRegistrationRequest($userFormData);
	        //the return value can be handled in many ways. Just want to simply display status
	        $status = $isRegistered ? 'ok' : 'problem';

	        //redirect. to remove the possibility to register twice
	        return $this->redirectToRoute('user_registered', array('status' => $status));
	    }

	    //render the form
		return $this->render('registration.html.twig', array(
			'form' => $form->createView(),
	    ));
	}

	/**
	* redirection after login
	* @param Request $request - to get params
	*/
	public function userRegistered(Request $request)
	{
		$registrationDone = $request->query->get('status') == 'ok';

		//messages can be handled in translations. Symfony has component for that.
		return $this->render('registered.html.twig', array(
			'title' => $registrationDone ? 'Success' : 'Problem occured',
			'message' => $registrationDone ? 'Thank you for your registration.' : 'Please try again. Something went wrong.',
		));			
	}


	/**
	* list all registered users
	* @param RequestManager $requestManager
	*/
	public function allUsers(RequestManager $requestManager)
	{
		$users = [];

		try {
			$response = $requestManager->getAllUsers();

			if($response && isset($response->status) && $response->status == 'ok') {
				$users = $response->message;
			}
		} catch (\GuzzleHttp\Exception\ClientException $e) {
			//TODO display some message in template
		}

		return $this->render('users.html.twig', array(
			'users' => $users
        ));	
	}
}
